<html>

<head>
    <title>Terms and Conditions</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"
        integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
</head>

<body>
    <style>
        #button {
            display: inline-block;
            background-color: #1b5880;
            width: 50px;
            height: 50px;
            text-align: center;
            border-radius: 4px;
            position: fixed;
            bottom: 30px;
            right: 30px;
            transition: background-color .3s,
                opacity .5s, visibility .5s;
            opacity: 0;
            visibility: hidden;
            z-index: 1000;
        }

        #button::after {
            content: "\f077";
            font-family: FontAwesome;
            font-weight: normal;
            font-style: normal;
            font-size: 2em;
            line-height: 50px;
            color: #fff;
        }

        #button:hover {
            cursor: pointer;
            background-color: #333;
        }

        #button:active {
            background-color: #555;
        }

        #button.show {
            opacity: 1;
            visibility: visible;
        }
    </style>
    <a id="button"></a>
    <div class="container">
        <p>These terms and conditions constitute the full and complete service agreement (the "Agreement") between you
            (the "Customer") and Elite Advance Building Cleaning Services L.L.C. ("Service"). Please take some time to
            review this Agreement.</p>
        <p>Definitions</p>
        <p>Normal House Cleaning Service: Tidy up, dusting, mopping, vacuuming, clean kitchen surfaces and floors, clean
            bathroom, cleaning indoor windows.</p>
        <p>Deep Cleaning Service: Clean outside windows, washing balconies, washing floors, clean inside cupboard,
            removing build up grease and dirt, reaching high areas with ladder.....etc.</p>
        <p>Disinfection Service: Spraying all surfaces, floors, kitchen, bathroom, furniture, glass, door, switches,
            pathways and up to man Hight , using approved DM chemicals.</p>
        <ol type="A">
            <li>
                <p>Cleaning services</p>
                <ol type="1">
                    <li>Subject to the terms of this Agreement, Elite Advance Building Cleaning Services L.L.C. agrees
                        to provide domestic, office cleaning, end of tenancy, move in, after party, after builders,
                        window cleaning or ironing services (the "Service") to the Customer at an address specified by
                        the Customer (the "Premises").</li>
                    <li>The Service will be for such cleaning duties as agreed with the Customer at the time of booking.
                    </li>
                    <li>Elite Advance Building Cleaning Services L.L.C. will provide one or more cleaners (the
                        "Cleaner") to attend the Premises to provide the Service at a time and date mutually agreed
                        between Elite Advance Building Cleaning Services L.L.C. and the Customer (the "Service Time").
                    </li>
                    <li>Elite Advance Building Cleaning Services L.L.C. endeavour to provide the Service faithfully,
                        diligently and in a timely and professional manner.</li>
                    <li>For Services such as End or Pre tenancy cleaning, after builders, move in, after party on
                        spring/deep cleaning Elite Advance Cleaning Services can provide cleaning materials and
                        equipment’s (upon request and subject to additional charge). All cleaning equipment’s are safe
                        and in full working order.</li>
                    <li>If any estimate is given on how long it will take our cleaners to do the job, this is only an
                        estimate based on the average time it takes to clean a home of similar size. It is difficult to
                        estimate precisely how long the job may take and a degree of flexibility may be required.</li>
                    <li>Our acceptance of your booking brings into existence a legally binding contract between us.</li>
                    <li>Elite Advance Cleaning Services will advise all Cleaners to keep clients keys safe at all times
                        and to not keep the Customers address attached to them; although Elite Advance Building Cleaning
                        Services L.L.C. does not take responsibility for any loss or damage should this occur.</li>
                    <li>Elite Advance Cleaning Services will endeavor to make every reasonable effort to replace your
                        preferred Cleaner in the event of sickness or holiday, subject to availability.</li>
                </ol>
            </li>
            <li>
                <p>Satisfaction Guarantee</p>
                <span>Your satisfaction is guaranteed. If you are not completely satisfied with any the standard of
                    service provided by the Cleaner, Elite Advance Cleaning Services will introduce them with a
                    replacement Cleaner as soon as possible,. Please contact the office as soon as possible during our
                    normal business hours in 24 hours.</span>
                <p>The Customer represents and warrants that:</p>
                <ol type="1">
                    <li>It will provide a safe working environment at the Premises for the Cleaner to perform the
                        Service;</li>
                    <li>The Cleaner will have unencumbered and unobstructed access to those areas of the Premises
                        requiring the Service;</li>
                    <li>It will provide the Cleaner with access to all services and utilities (including hot and cold
                        water, electricity, and rubbish bins) as required by the Cleaner to provide the Service;</li>
                    <li>The client agrees to behave nicely to the Cleaner and to treat her/him with respect;</li>
                    <li>It will advise Elite Advance Cleaning Services prior to the commencement of the Service of any
                        hazards, slippery surfaces, risks or dangers, ingrained dirt, grease or grime at the Premises;
                    </li>
                    <li>It is authorized to use the Premises and obtain the provision of Service;</li>
                    <li>If the Customer requires the Cleaner to clean behind or under any heavy items (e.g. a fridge,
                        bookshelf, or other furniture), it will move those items prior to the commencement of the
                        Service; and it will secure or remove any fragile, delicate, breakable or valuable items,
                        including cash, jewelry, works of art, antiques, or items of sentimental value prior to the
                        commencement of the Service.</li>
                    <li>Fridges and Freezers must be thoroughly defrosted before cleaning can start. Kitchen cupboards
                        must be emptied before cleaning can start. They will not be covered by the terms if this is not
                        the case. Ovens must be in a condition that will enable thorough cleaning with standard
                        professional chemical products.</li>
                    <li>We will do our best to make sure your electrical appliances, microwave, oven, fridge/freezer,
                        are cleaned to a high standard. However, if they have not been cleaned since they were purchased
                        we won’t be held liable for ingrained dirt that cannot be shifted using standard professional
                        chemicals.</li>
                    <li>The cleaner is not responsible of segregating the laundry before washing. If for any reason
                        discoloration or spots appear, Elite Advance will not be held responsible.</li>
                    <li>The Customer agrees to fully instruct/show (included the materials) to the Cleaner how to use
                        machines (such as washing machine, dryer, iron or any other) if service require.</li>
                    <li>A charge (no refund) appears for you if the Customer is away and don’t inform us that won’t need
                        Service.</li>
                </ol>
            </li>
            <li>
                <p>Health and safety risks</p>
                <ol type="1">
                    <li>the Cleaner may, either before or during the provision of the Service not use or cease using any
                        materials or cleaning equipment provided by the Customer if
                        The Cleaner thinks, in their absolute discretion, that the use of such materials or cleaning
                        equipment poses a risk to health and safety.</li>
                    <li>The Cleaner may, either before or during the provision of the Service not provide or cease the
                        provision of the Service where carrying out the Service presents, in the absolute discretion of
                        the Cleaner, a risk to health and safety.</li>
                    <li>The Cleaner may, either before or during the provision of the Service not provide or cease the
                        clean of Animal and Human waste. Waste is not just an urban menace: it is an environmental
                        pollutant and a human health hazard! Animal waste can contain bacteria, parasites, and pathogens
                        that can directly and indirectly cause people to get sick.</li>
                </ol>
            </li>
            <li>
                <p>No engagement of cleaners</p>
                <ol type="1">
                    <li>The Customer acknowledges Elite Advance Cleaning Services invest significant resources in
                        recruiting, selecting and training its Cleaners. Unless Elite Advance Cleaning Services give
                        prior written permission, the Customer must not, directly or indirectly, engage, employ or
                        contract with any Cleaner to provide domestic services to the Customer or any associate of the
                        customer for any period during which services are provided by Elite Advance Cleaning Services or
                        for a period within 12 months after the conclusion of any Service.</li>
                    <li>The Customer acknowledges that Elite Advance Cleaning Services may suffer loss and damage,
                        including, without limitation consequential loss, as a result of a breach of this clause by the
                        Customer.</li>
                    <li>The Customer acknowledges that have to pay Elite Advance Cleaning Services the amount of AED
                        28,000 if employ the Cleaner direct or indirect within 12 months after the conclusion of the
                        Agreement. Elite Advance Cleaning Services keeps the rights to start legal procedure against the
                        Customer.</li>
                </ol>
            </li>
            <li>
                <p>Bookings</p>
                <ol type="1">
                    <li>Our cleaning service may be ordered by telephone, e-mail or online and you agree to be bound by
                        these terms and conditions.</li>
                    <li>At the time of booking the Customer must provide details of any hazards, slippery surfaces,
                        risks or dangers, ingrained dirt, grease or grime located at the Premises;</li>
                    <li>Elite Advance Cleaning Services provide all quotations at the time of booking, quotation will be
                        sent via email to the Customer (if email address is provided);</li>
                    <li>Elite Advance Cleaning Services reserve the right not to accept a booking for any reason.</li>
                    <li>These terms and conditions shall be governed by the relevant United Arab Emirates law, and by
                        agreeing to be bound by them the customer agrees to submit to the exclusive jurisdiction of the
                        relevant courts of the United Arab Emirates. Elite Advance Cleaning Services reserves the right
                        to make any changes to any part of these terms and conditions without giving any prior notice.
                        Please check the website for updates.</li>
                    <li>Domestic Cleaning: A minimum of 2 hours per cleaning visit applies.</li>
                    <li>One off /Spring Cleaning/After party: A minimum of 4 hours per cleaning visit applies.</li>
                    <li>End of Tenancy Cleaning: We have a fixed prices for this service depending on size of the
                        property;</li>
                    <li>After builders cleaning we will ask you for a “list to do” and then we can discuss how many
                        hours are necessary to complete the job for you.</li>
                </ol>
            </li>
            <li>
                <p>Payment terms</p>
                <ol type="1">
                    <li>The Customer agrees to pay the price quoted by Elite Advance Cleaning Services.</li>
                    <li>The cleaners will be paid by Elite Advance Cleaning Services;</li>
                    <li>
                        <p>We accept the following payment methods from The Customer:</p>
                        <p>- Bank transfer; weekly/fortnightly or monthly Contracts. We also accept debit or credit card
                            (subject to additional charge). We can accept cash in limited cases and only after the
                            authorization by Elite Advance Cleaning Services;</p>
                    </li>
                    <li>The Customer agrees to pay Elite Advance Cleaning Services for every hour of service carried by
                        the Cleaner.</li>
                    <li>Elite Advance Cleaning prepares contract invoices on the end of each month for services rendered
                        in the previous month;</li>
                    <li>Elite Advance Cleaning Services reserves the rights to charge the Customer a late payment fee of
                        10% for any overdue invoices; 20% for delay more than 10 days; 30% for delay more than 20 days;
                    </li>
                    <li>Elite Advance Cleaning reserves the rights to stop with immediate effect the cleaning services
                        provided to the Customer in case of no payment received or delayed payment.</li>
                    <li>The keys are returned within five working days after the invoices have been paid in full.</li>
                </ol>
            </li>
            <li>
                <p>Non Payment</p>
                <p>Elite Advance Cleaning Services will collect any outstanding monies owed to us. If as a result we
                    have to use a county court to secure payment, you agree to pay any debt collecting fees, court fees,
                    legal cost, or interest that will occur due to the result of non payment of your outstanding bill.
                </p>
            </li>
            <li>
                <p>Complaints</p>
                <p>If the Customer is dissatisfied for any reason with the Service provided, it must inform Elite
                    Advance Cleaning Services within 24 hours of completion of the Service. Elite Advance Cleaning
                    Services strives to achieve 100% customer satisfaction and will endeavour to resolve the problem
                    quickly and efficiently. We will not consider any complaints that are notified after a period of 24
                    hours.</p>
                <p>We will not be held liable for work not completed, or not completed to a good standard, if other
                    people are present in the property when our cleaners are working and carrying out the job.</p>
            </li>
            <li>
                <p>Exclusions and limitations</p>
                <p>Elite Advance Cleaning Services is not responsible for:</p>
                <ol type="1">
                    <li>not completing or providing the Service as a result of a breach of a warranty by the Customer
                        (including a failure by the Customer to provide a safe working environment or unencumbered
                        access to the Premises);</li>
                    <li>not completing or providing the Service as a result of the Cleaner not proceeding for health and
                        safety reasons;</li>
                    <li>any loss or damage incurred by the Customer or any third party as a result of the effects of a
                        force majeure, being any event beyond the reasonable control of Elite Advance Cleaning Services;
                    </li>
                    <li>not completing or providing the Service due to an act or omission of the Customer or any other
                        person at the Premises during provision of the Service;</li>
                    <li>existing dirt, wear, damage or stains that cannot be completely cleaned or removed;</li>
                    <li>any wear or discolouring of fabric or surfaces becoming more visible once dirt has been removed;
                        7. All fragile and highly breakable items, cash, jewellery, items of sentimental value, art and
                        antiques.</li>
                    <li>The cost of any key replacement or locksmith fees, unless keys were lost by Elite Advance
                        Cleaning Services or the Cleaner.</li>
                    <li>Old stains that cannot be removed using normal cleaning methods.</li>
                    <li>Accidental damage due to faulty equipment.</li>
                    <li>Any accidental damage caused by a cleaner working for Elite Advance Cleaning Services, if there
                        is an outstanding amount owed to Elite Advance Cleaning Services (excluding payment due for the
                        cleaning visit when the accident happened).</li>
                </ol>
            </li>
            <li>
                <p>Accidents, breakage, damage & theft</p>
                <ol type="1">
                    <li>While our cleaners will treat your home with great care accidents can and do happen from time to
                        time. Elite Advance Cleaning Services have public liability insurance. The policy will cover
                        major accidental damage caused by our cleaners.</li>
                    <li>The Customer must inform Elite Advance Cleaning Services of any incident where an accident,
                        breakage, damage to property has occurred due to any act of the Cleaner within 24 hours of
                        completion of the Service.</li>
                    <li>Any claims reported later than 24 hours after the clean will not be considered. If a report of
                        damage is made on a Friday it must be reported by Saturday 12:00 pm to be accepted as a valid
                        claim.</li>
                    <li>All fragile and highly breakable items must be secured or removed. Items excluded from liability
                        are: cash, jewellery, items of sentimental value, art and antiques.</li>
                    <li>We may require entry to the location of the claim within 24 hours to correct or assess the
                        problem.</li>
                </ol>
            </li>
            <li>
                <p>Cancellation</p>
                <ol type="1">
                    <li>The Customer must provide Elite Advance Cleaning Services with at least 24 hours notice prior to
                        the Service Time, if they wish to suspend, postpone or cancel the Service for any reason.</li>
                    <li>Domestic cleaning: You agree to pay the 50% price of the cleaning visit if you cancel or change
                        the date/time less than 24 hours prior to the scheduled appointment. You agree to pay the full
                        price of the cleaning visit in the event caused by our cleaners being turned away for any
                        reason; no one home to let them in; or a problem with your keys. If keys are provided they must
                        open the lock without any special efforts or skills.</li>
                </ol>
            </li>
            <li>
                <p>Cancellation by us</p>
                <span>We reserve the right to cancel the contract between us if:</span>
                <ol type="1">
                    <li>we have insufficient staff to fulfil the booking you have ordered;</li>
                    <li>we do not cover your area;</li>
                    <li>one or more of the services you ordered was listed at an incorrect price due to a typographical
                        error.</li>
                    <li>If we do cancel your contract we will notify you by e-mail within 7 days of your booking.</li>
                    <li>Notwithstanding the foregoing, nothing in these terms and conditions is intended to limit any
                        rights you might have as a consumer neither under applicable local law or other statutory rights
                        that may not be excluded nor in any way to exclude or limit our liability to you for any death
                        or personal injury resulting from our negligence.</li>
                    <li>By entering into a contract with Elite Advance Cleaning Services, you agree that after the
                        termination of the cleaning service you will not hire or use any domestic services provided by a
                        present or past cleaner introduced to you by Elite Advance Cleaning Services, Elite Advance will
                        open a case in UAE courts of illegal hiring of our cleaner.</li>
                </ol>
            </li>
            <li>
                <p>Ownership of rights</p>
                <span>All rights, including copyright, in this website are owned by or licensed to Elite Advance
                    Cleaning Services. Any use of this website or its contents, including copying or storing it or them
                    in whole or part, other than for your own personal, non commercial use, is prohibited without our
                    permission. You may not modify, distribute or repost anything on this website for any
                    purpose.</span>
            </li>
            <li>
                <p>Accuracy of content</p>
                <span>We have taken care in the preparation of the content of this website, in particular to ensure that
                    prices quoted are correct at the time of publishing and that all services have been fairly
                    described.</span>
            </li>
            <li>
                <p>Availability</p>
                <span>All services are subject to acceptance and availability. If the service you have booked is not
                    available, we will contact you by e-mail or phone (if you have given us details). You will have the
                    option either to wait until the service is available or to cancel your booking.</span>
            </li>
            <li>
                <p>Price</p>
                <span>The prices payable for services that you book are as set out in our website, Elite Advance has the
                    right to change applicable contract prices given 7 days’ notice. Extra Fee is of AED 5 applies on
                    cash bookings.</span>
            </li>
            <li>
                <p>Contracts</p>
                <span>While Contract terms and conditions are mentioned in each contract between Elite Advance and the
                    client, the following serves as addition and over writes any other contradiction term within the
                    contract</span>
                <ol type="1">
                    <li>Elite will cover Cleaner absence, refusal to work only, Elite will not cover any sick days or
                        public holidays set by UAE that result in cleaner absence, the Client has to pay for the above
                        mentioned days fully.</li>
                    <li>Elite will provide medical insurance to cleaners as per UAE law of each emirates, Elite Advance
                        has direct employers under Company sponsorship, also have other employers under man power supply
                        agencies sponsorship, Elite has also Nannies under Tadbeer Sponsorship.
                    </li>
                </ol>
            </li>
            <li>
                <p>Elite Advance policy</p>
                <span>The Customer acknowledges that any information provided by the Customer may be used by Elite
                    Advance Cleaning Services for the purpose of providing the Service. Elite Advance Cleaning Services
                    agree not to share any information provided by the Customer with any third party not directly
                    involved in the provision of the Service (unless required to do so by law).</span>
            </li>
            <li>
                <p>Law, jurisdiction and language</p>
                <span>This website, any content contained therein and any contract brought into being as a result of
                    usage of this website are governed by and construed in accordance with English law. Parties to any
                    such contract agree to submit to the exclusive jurisdiction of the courts of UAE. All contracts are
                    concluded in English.</span>
            </li>
            <li>
                <p>Changes to this agreement</p>
                <span>Elite Advance Cleaning Services reserve the right to update or modify these terms and conditions
                    at any time without prior notice, and may do so by publishing an updated agreement on its website.
                    Each updated agreement will take effect 24 hours after it has been published on the website.</span>
            </li>
        </ol>
    </div>
    <script>
        var btn = $('#button');
        $(window).scroll(function() {
            if ($(window).scrollTop() > 300) {
                btn.addClass('show');
            } else {
                btn.removeClass('show');
            }
        });

        btn.on('click', function(e) {
            e.preventDefault();
            $('html, body').animate({
                scrollTop: 0
            }, '300');
        });
    </script>
</body>

</html>
