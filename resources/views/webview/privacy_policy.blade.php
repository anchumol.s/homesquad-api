<html>

<head>
    <title>Privacy Policy</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"
        integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
</head>

<body>
    <style>
        #button {
            display: inline-block;
            background-color: #1b5880;
            width: 50px;
            height: 50px;
            text-align: center;
            border-radius: 4px;
            position: fixed;
            bottom: 30px;
            right: 30px;
            transition: background-color .3s,
                opacity .5s, visibility .5s;
            opacity: 0;
            visibility: hidden;
            z-index: 1000;
        }

        #button::after {
            content: "\f077";
            font-family: FontAwesome;
            font-weight: normal;
            font-style: normal;
            font-size: 2em;
            line-height: 50px;
            color: #fff;
        }

        #button:hover {
            cursor: pointer;
            background-color: #333;
        }

        #button:active {
            background-color: #555;
        }

        #button.show {
            opacity: 1;
            visibility: visible;
        }
    </style>
    <a id="button"></a>
    <div class="container">
        <p>We are committed to safeguarding the privacy of our website & mobile apps visitors; this policy sets out how
            we will treat your personal information. Our website uses cookies. By using our website & mobile apps and
            agreeing to this policy, you consent to our use of cookies in accordance with the terms of this policy. What
            information do we collect? We may collect, store and use the following kinds of personal information:
        <ul>
            <li>Information about your computer and about your visits to and use of this website (including your IP
                address, geographical location, browser type and version, operating system, referral source, length of
                visit, page views and website navigation);</li>
            <li>Information relating to any transactions carried out between you and us on or in relation to this
                website & mobile apps, including information relating to any purchases you make of our goods or services
                (including booking a cleaner through EliteMaids);</li>
            <li>Information that you provide to us for the purpose of registering with us (including your name, address
                and email address);</li>
            <li>Information that you provide to us for the purpose of subscribing to our website & mobile apps services,
                email notifications and/or newsletters (including your name and email address)
                Any other information that you choose to send to us; and other information.</li>
        </ul>
        </p>
        <p>Before you disclose to us the personal information of another person, you must obtain that person’s consent
            to both the disclosure and the processing of that personal information in accordance with the terms of this
            privacy policy.</p>
        <p>All credit/debit cards details and personally identifiable information will NOT be stored, sold, shared,
            rented or leased to any third parties.</p>
        <ol>
            <li>
                <p>Cookies</p>
                <p>A cookie is a file containing an identifier (a string of letters and numbers) that is sent by a web
                    server to a web browser and is stored by the browser. The identifier is then sent back to the server
                    each time the browser requests a page from the server. This enables the web server to identify and
                    track the web browser. We may use both “session” cookie and “persistent” cookies on the website.
                    Session cookies will be deleted from your computer when you close your browser. Persistent cookies
                    will remain stored on your computer until deleted, or until they reach a specified expiry date. We
                    will use the session cookies to: keep track of you whilst you navigate the website; keep track of
                    your bookings; prevent fraud and increase website security; and [other uses]. We will use the
                    persistent cookies to: enable our website to recognize you when you visit; keep track of your
                    preferences in relation to your use of our website; and other uses.</p>
                <p>We use Google Analytics to analyse the use of this website. Google Analytics generates statistical
                    and other information about website use by means of cookies, which are stored on users’ computers.
                    The information generated relating to our website & mobile apps is used to create reports about the
                    use of the website & mobile apps. Google will store this information. Google’s privacy policy is
                    available at: <a
                        href="http://www.google.com/privacypolicy.html">http://www.google.com/privacypolicy.html</a>.
                    Our payment services providers may also send
                    you cookies.</p>
            </li>
            <li>
                <p>Using Your Personal Information</p>
                <p>Personal information submitted to us via this website & mobile apps will be used for the purposes
                    specified in this privacy policy or in relevant parts of the website. We may use your personal
                    information to:</p>
                <ol type="a">
                    <li>administer the website;</li>
                    <li>improve your browsing experience by personalizing the website;</li>
                    <li>enable your use of the services available on the website;</li>
                    <li>supply to you services purchased via the website;</li>
                    <li>send statements and invoices to you, and collect payments from you;</li>
                    <li>send you general (non-marketing) commercial communications;</li>
                    <li>send you email notifications which you have specifically requested;</li>
                    <li>send you our newsletter and other marketing communications relating to our business which we
                        think may be of interest to you, by post or, where you have specifically agreed to this, by
                        email or similar technology (and you can inform us at any time if you no longer require
                        marketing communications);</li>
                    <li>deal with inquiries and complaints made by or about you relating to the website & mobile apps;
                        keep the website & mobile apps secure and prevent fraud;</li>
                    <li>verify compliance with the terms and conditions governing the use of the website (including
                        monitoring private messages sent through our website private messaging service); and
                        other uses.</li>
                </ol>
                <p>Where you submit personal information for publication on our website & mobile apps, we will publish
                    and otherwise use that information in accordance with the license you grant to us. We will not,
                    without your express consent, provide your personal information to any third parties for the purpose
                    of direct marketing. EliteMaids will neither store any debit/credit card details of users nor pass
                    any debit/credit card details to third parties.</p>
            </li>
            <li>
                <p>Disclosures</p>
                <p>We may disclose your personal information to any of our employees, officers, agents, suppliers or
                    subcontractors insofar as reasonably necessary for the purposes set out in this privacy policy. We
                    may disclose your personal information to any member of our group of companies (this means our
                    subsidiaries, our ultimate holding company and all its subsidiaries) insofar as reasonably necessary
                    for the purposes set out in this privacy policy. In addition, we may disclose your personal
                    information:</p>
                <ol type="a">
                    <li>to the extent that we are required to do so by law;</li>
                    <li>in connection with any ongoing or prospective legal proceedings;</li>
                    <li>in order to establish, exercise or defend our legal rights (including providing information to
                        others for the purposes of fraud prevention and reducing credit risk);</li>
                    <li>to any person who we reasonably believe may apply to a court or other competent authority for
                        disclosure of that personal information where, in our reasonable opinion, such court or
                        authority would be reasonably likely to order disclosure of that personal information.</li>
                </ol>
                <p>Except as provided in this privacy policy, we will not provide your information to third parties.</p>
            </li>
            <li>
                <p>International Data Transfers</p>
                <p>Information that we collect may be stored and processed in and transferred between any of the
                    countries in which we operate in order to enable us to use the information in accordance with this
                    privacy policy. In addition, personal information that you submit for publication on the website may
                    be published on the internet and may be available, via the internet, around the world. We cannot
                    prevent the use or misuse of such information by others. You expressly agree to such transfers of
                    personal information.</p>
            </li>
            <li>
                <p>Security of Your Personal Information</p>
                <p>We will take reasonable technical and organizational precautions to prevent the loss, misuse or
                    alteration of your personal information. We will store all the personal information you provide on
                    our secure (password- and firewall-protected) servers.</p>
                <p>All electronic transactions entered into via the website will be protected by encryption technology.
                </p>
                <p>You acknowledge that the transmission of information over the internet is inherently insecure, and we
                    cannot guarantee the security of data sent over the internet. EliteMaids takes appropriate steps to
                    ensure data privacy and security including through various hardware and software methodologies.
                    However, EliteMaids cannot guarantee the security of any information that is disclosed online.</p>
            </li>
            <li>
                <p>Policy Amendments</p>
                <p>We may update this privacy policy from time to time by posting a new version on our website. You
                    should check this page occasionally to ensure you are happy with any changes. We may also notify you
                    of changes to our privacy policy by email.</p>
            </li>
        </ol>
    </div>
    <script>
        var btn = $('#button');
        $(window).scroll(function() {
            if ($(window).scrollTop() > 300) {
                btn.addClass('show');
            } else {
                btn.removeClass('show');
            }
        });

        btn.on('click', function(e) {
            e.preventDefault();
            $('html, body').animate({
                scrollTop: 0
            }, '300');
        });
    </script>
</body>

</html>
