<html>

<head>
    <title>Checkout</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"
        integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.checkout.com/js/framesv2.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
</head>

<body>
    <style>
        body {
            background-color: #ffffff;
        }

        .modal-backdrop {
            background-color: #ffffff;
        }

        .card-frame {
            border: solid 1px #5f467f98;
            border-radius: 3px;
            margin-bottom: 10px;
            height: 40px;
            box-shadow: 0 1px 3px 0 rgba(19, 57, 94, 0.2);
        }

        #pay-button {
            border-color: #999;
            color: #FFF;
            background-color: #5F467F;
            box-shadow: 0 1px 3px 0 rgba(19, 57, 94, 0.4);
            border-radius: 7px;
            width: 100%;
        }

        #pay-button:disabled {
            border-color: #999999a4;
            background-color: #DDD;
            color: #999;
            box-shadow: none;
        }

        #amount-button {
            border-color: #e6e6e6;
            color: #303030;
        }
    </style>
    <div class="col-12">
        <!-- Modal -->
        <div class="modal fade" id="checkoutModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5">Make Payment</h1>
                    </div>
                    <div class="modal-body mt-4 mb-3 pb-0">
                        <form id="checkout-form" method="POST"
                            action="{{ url('payment/gateway/checkout/go/' . $booking->booking_id) }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="checkout_primary_key"
                                value="{{ Config::get('values.checkout_primary_key') }}">
                            <input type="hidden" name="token">
                            <input type="hidden" name="reference" value="{{ $booking->reference_id }}">
                            <input type="hidden" name="amount" value="{{ (int) ($input['amount'] * 100) }}">
                            <input type="hidden" name="currency" value="{{ Config::get('values.currency_code') }}">
                            <input type="hidden" name="name" value="{{ $customer->customer_name }}">
                            <input type="hidden" name="customer_tel" value="{{ $customer->mobile_number_1 }}">
                            <input type="hidden" name="email" value="{{ $customer->email_address }}">
                            <input type="hidden" name="booking_id" value="{{ $booking->booking_id }}">
                            <div class="one-liner">
                                <div class="card-frame">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-auto me-auto"><button type="button"
                                        class="btn btn-outline-secondary ml-0" id="amount-button"
                                        disabled>AED&nbsp;<b>{{ number_format($input['amount'], 2) }}</b></button>
                                </div>
                                <div class="col-auto"><button type="submit" class="btn btn-info pl-4 pr-4"
                                        id="pay-button" disabled>PAY NOW</button></div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            var myModal = new bootstrap.Modal(document.getElementById('checkoutModal'));
            var payButton = document.getElementById("pay-button");
            myModal.show()
            Frames.init({
                publicKey: $('#checkout-form input[name="checkout_primary_key"]').val(),
                style: {
                    base: {
                        color: 'black',
                    },
                    autofill: {
                        backgroundColor: 'yellow',
                    },
                    hover: {
                        color: 'blue',
                    },
                    focus: {
                        color: 'blue',
                    },
                    valid: {
                        color: 'green',
                    },
                    invalid: {
                        color: 'red',
                    },
                    placeholder: {
                        base: {
                            color: 'gray',
                        },
                    },
                },
            });
            Frames.addEventHandler(
                Frames.Events.CARD_VALIDATION_CHANGED,
                function(event) {
                    console.log("CARD_VALIDATION_CHANGED: %o", event);
                    payButton.disabled = !Frames.isCardValid();
                }
            );
            Frames.addEventHandler(
                Frames.Events.CARD_TOKENIZED,
                function(event) {
                    $('#checkout-form input[name="token"]').val(event.token);
                    $("#checkout-form").submit();
                }
            );
            var checkout_form = document.getElementById("checkout-form");
            if (checkout_form) {
                checkout_form.addEventListener("submit", function(event) {
                    payButton.disabled = true;
                    $('#pay-button').html(`Please wait...`);
                    event.preventDefault();
                    Frames.submitCard();
                });
            }
        });
    </script>
</body>

</html>
