<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\CustomerNotifications;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class CustomerApiNotificationsController extends Controller
{
    public function notifications(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = Config::get('values.debug_customer_id');
        }
        /************************************************************* */
        $input = @$data['params'];
        /************************************************************* */
        $response['status'] = 'success';
        $response['notification_list'] = DB::table('customer_notifications as cn')
            ->select(
                'cn.id as notification_id',
                'cn.customer_id',
                'b.reference_id as booking_ref_id',
                'b.booking_id',
                'cn.created_at',
                'cn.content as message_details',
                DB::raw('(CASE WHEN cn.read_at IS NOT NULL THEN true ELSE false END) as read_status'),
                DB::raw('null as icon_url')
            )
            ->leftJoin('bookings as b', 'cn.booking_id', 'b.booking_id')
            ->where([['cn.cleared_at', '=', null], ['cn.deleted_at', '=', null]])
            ->where(function ($query) use ($input) {
                $query->where([['cn.customer_id', '=', @$input['id']]]);
                $query->orWhere([['cn.customer_id', '=', null]]);
            })
            ->where(function ($query) use ($input) {
                $query->where([['cn.expired_at', '=', null]]);
                $query->orWhere([['cn.expired_at', '>=', date('Y-m-d')]]);
            })
            ->orderBy('cn.id', 'DESC')
            ->get();
        /******************************************************************************* */
        if (@$input['id']) {
            // templating starts
            $customer = Customer::where('customer_id', '=', $input['id'])->first();
            foreach ($response['notification_list'] as $key => $notification) {
                $response['notification_list'][$key]->read_status = $notification->read_status ? true : false;
                $response['notification_list'][$key]->time_elapsed = time_elapsed_string($notification->created_at);
                unset($response['notification_list'][$key]->created_at);
                $template_data = array(
                    'customer_name' => $customer->customer_name,
                    'company_name' => Config::get('values.company_name'),
                    'booking_ref_id' => $notification->booking_ref_id ?: $notification->booking_id,
                );
                unset($response['notification_list'][$key]->booking_ref_id);
                foreach ($template_data as $temp => $value) {
                    $response['notification_list'][$key]->message_details = str_replace('{{' . $temp . '}}', '<b>' . $value . '</b>', $response['notification_list'][$key]->message_details);
                }
            }
        }
        /******************************************************************************* */
        $response['message'] = sizeof($response['notification_list']) ? "Notifications fetched successfully." : "No notifications found.";
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
    public function all_notification_read(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = 1; // match with middleware for test
        }
        /************************************************************* */
        $input = @$data['params'];
        /************************************************************* */
        $response['status'] = 'success';
        CustomerNotifications::where([['customer_id', '=', $input['id']], ['deleted_at', '=', null], ['read_at', '=', null]])->update(['read_at' => Carbon::now()]);
        $response['message'] = "All notifications marked as read.";
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
    public function clear_notiication(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = 1; // match with middleware for test
        }
        /************************************************************* */
        $input = @$data['params'];
        /************************************************************* */
        $response['status'] = 'success';
        CustomerNotifications::where([['customer_id', '=', $input['id']], ['deleted_at', '=', null], ['read_at', '!=', null], ['cleared_at', '=', null]])->update(['cleared_at' => Carbon::now()]);
        $response['message'] = "All notifications cleared.";
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
    public function fcm_test(Request $request)
    {
        $data = [
            'to' => @$request->token,
            'notification' => [
                'title' => "FCM Test",
                'body' => "This is sample content",
            ],
            "data" => [
                "click_action" => 'test click_action',
                "screen" => @$request->screen ?: null,
            ],
        ];
        $data = json_encode($data);
        //FCM API end-point
        $url = 'https://fcm.googleapis.com/fcm/send';
        //api_key in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
        //header with content_type api key
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key=' . Config::get('values.google_fcm_server_key'),
        );
        //CURL request to route notification to FCM connection server (provided by Google)
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        if ($result === false) {
            die('Oops! FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
        dd($result);
    }
}
