<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\CouponCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;
use stdClass;

class CustomerApiCouponController extends Controller
{
    public function coupon_apply(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = 1; // match with middleware for testing
            $data['params']['promocode'] = 'TP20';
            $data['params']['start_time'] = "13/06/2023";
            $data['params']['hours'] = 2;
        }
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $validator = Validator::make(
            (array) $input,
            [
                'promocode' => 'required|string',
                'start_time' => 'required|date_format:d/m/Y',
                'hours' => 'required|numeric|min:1',
            ],
            [],
            [
                'promocode' => 'Promo Code',
                'start_time' => 'Start Time',
                'hours' => 'Hours',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        $input['start_time'] = Carbon::createFromFormat('d/m/Y', $input['start_time'])->format('Y-m-d');
        /************************************************************* */
        $coupon = CouponCode::where([['coupon_name', '=', $input['promocode']], ['status', '=', 1]])->first();
        if ($coupon) {
            $booking = Booking::where([['customer_id', '=', $input['id']], ['booking_status', '=', 1]])->first();
            if ($input['start_time'] > $coupon->expiry_date) {
                return Response::json(array('result' => array('status' => 'failed', 'message' => 'Expired coupon code.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            } else if ($booking && $coupon->coupon_type == "FT") {
                return Response::json(array('result' => array('status' => 'failed', 'message' => 'Coupon valid for first time booking only.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            } else if ($input['hours'] < $coupon->min_hrs) {
                return Response::json(array('result' => array('status' => 'failed', 'message' => 'Coupon only for ' . $coupon->min_hrs . ' hours working hour(s).'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            $valid_week_days = explode(',', $coupon->valid_week_day);
            $week_day = Carbon::createFromFormat('Y-m-d', $input['start_time'])->format('N') - 1;
            if (!in_array($week_day, $valid_week_days)) {
                return Response::json(array('result' => array('status' => 'failed', 'message' => 'Coupon not valid for this day.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            // valid coupon found
            $response['status'] = 'success';
            $response['message'] = 'Coupon validated successfully.';
            $response['validity'] = true;
            $response['is_valid'] = true;
            $response['code_details'] = new stdClass();
            $response['code_details']->code_details = $coupon->coupon_name;
            $response['code_details']->code_id = $coupon->coupon_id;
            $response['code_details']->promocode = $coupon->coupon_name;
            $response['code_details']->validity = $coupon->expiry_date ?: "unlimited";
            $response['code_details']->discount_type = $coupon->discount_type == 0 ? "percentage" : "amount";
            $response['code_details']->amount = $coupon->percentage;
            $response['code_details']->limited = true;
            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        } else {
            return Response::json(array('result' => array('status' => 'failed', 'message' => 'Invalid coupon code.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
    public function coupon_list(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
        }
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        /************************************************************* */
        $response['status'] = 'success';
        $response['available_codelist'] = CouponCode::select(
            'coupon_id as code_id',
            'coupon_name as promocode',
            'coupon_name as code_details',
            'expiry_date as validity',
            DB::raw('"any" as coupon_type'),
            DB::raw('(CASE WHEN discount_type = 0 THEN "percentage" ELSE "amount" END) as discount_type'),
            DB::raw('(CASE WHEN coupon_type = "FT" THEN true ELSE false END) as limited'),
        )
            ->where([['status', '=', 1]])
            ->orderBy('expiry_date', 'DESC')
            ->get();
        $response['message'] = sizeof($response['available_codelist']) ? "Coupons fetched successfully." : "No coupons found.";
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
}
