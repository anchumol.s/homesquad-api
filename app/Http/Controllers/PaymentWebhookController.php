<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Customer;
use Config;
use Illuminate\Http\Request;
use Mail;

class PaymentWebhookController extends Controller
{
    public function tamara_order_approved(Request $request)
    {
        // "webhook_id": "cde573f6-55fb-49ad-ba8f-e4467495877c",
        // first validate the webhook is from tamara or not
        /*list($header, $payload, $signature) = explode('.', $request->bearerToken);
        $jsonToken = base64_decode($payload);
        $tamara_api_token = json_decode($jsonToken, true);
        if($tamara_api_token != Config::get('values.tamara_api_token')){
            // means it's not from tamara
            Mail::send(
                'emails.debug_mail',
                [
                    'text' => json_encode($request->all())
                ],
                function ($m) {
                    $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                    $m->subject('Tamara Test');
                    $m->to('samnad.s@azinova.info', 'Developer Test');
                }
            );
            return "Unauthorized Access !";
        }*/
        /**************************************************************************************************** */
        if (@$request['order_status'] == "approved") {
            // got 'approved' order_status from tamara
            /**************************************************************************************************** */
            // notify back to tamra that we have authorised the payment
            $order_id = $request['order_id'];
            $order_reference_id = $request['order_reference_id'];
            $post_header = array("Authorization" => "Bearer " . Config::get('values.tamara_api_token'), "Content-Type" => "application/json", "Accept" => "application/json");
            $client = new \GuzzleHttp\Client([
                'verify' => false,
            ]);
            $responseFull = $client->request('POST', Config::get('values.tamara_base_url') . "orders/" . $order_id . "/authorise", [
                'headers' => $post_header,
            ]);
            $authorise_data = json_decode((string) $responseFull->getBody(), true);
            /**************************************************************************************************** */
            // get order details
            $post_header = array("Authorization" => "Bearer " . Config::get('values.tamara_api_token'), "Content-Type" => "application/json", "Accept" => "application/json");
            $client = new \GuzzleHttp\Client([
                'verify' => false,
            ]);
            $responseFull = $client->request('GET', Config::get('values.tamara_base_url') . "orders/" . $order_id, [
                'headers' => $post_header,
            ]);
            $order_data = json_decode((string) $responseFull->getBody(), true);
            /**************************************************************************************************** */
            // notify customer and admin
            $booking = Booking::where('reference_id', $order_data['order_reference_id'])->first();
            $customer = Customer::where('customer_id', '=', $booking->customer_id)->first();
            send_booking_confirmation_mail_to_any($booking->booking_id);
            send_booking_confirmation_sms_to_customer($booking->booking_id);
            pushNotification($customer, ['title' => "Booking Received !", 'body' => "Booking received with Ref. Id " . $booking->reference_id . "."], ['screen' => 'BOOKING_HISTORY']);

        }
        /**************************************************************************************************** */
        Mail::send(
            'emails.debug_mail',
            [
                'text' => json_encode($request->all())
            ],
            function ($m) {
                $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                $m->subject('Tamara Test');
                $m->to('samnad.s@azinova.info', 'Developer Test');
            }
        );
    }
}
