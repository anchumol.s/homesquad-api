<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceTypeCharges extends Model
{
    use HasFactory;
    protected $table = 'service_type_charges';
    public $timestamps = true;
    protected $primaryKey = 'id';
}
