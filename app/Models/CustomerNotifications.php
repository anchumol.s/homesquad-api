<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerNotifications extends Model
{
    use HasFactory;
    protected $table = 'customer_notifications';
    public $timestamps = true;
    protected $primaryKey = 'id';
    protected $casts = [
        'read_at' => 'boolean',
    ];
}
