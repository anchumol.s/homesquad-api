<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MaidLeave extends Model
{
    use HasFactory;
    protected $table = 'maid_leave';
    public $timestamps = false;
    protected $primaryKey = 'leave_id';
}
