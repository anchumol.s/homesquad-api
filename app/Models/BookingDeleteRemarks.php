<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingDeleteRemarks extends Model
{
    use HasFactory;
    protected $table = 'booking_delete_remarks';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
