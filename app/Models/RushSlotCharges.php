<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RushSlotCharges extends Model
{
    use HasFactory;
    protected $table = 'rush_slot_charges';
    public $timestamps = true;
    protected $primaryKey = 'id';
}