<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceTypeCategories extends Model
{
    use HasFactory;
    protected $table = 'service_type_categories';
    public $timestamps = true;
    protected $primaryKey = 'id';
}
