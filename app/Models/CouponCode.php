<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CouponCode extends Model
{
    use HasFactory;
    protected $table = 'coupon_code';
    public $timestamps = false;
    protected $primaryKey = 'coupon_id';
    protected $casts = [
        'limited' => 'boolean',
    ];
}
