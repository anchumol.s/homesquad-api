<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerWalletCredits extends Model
{
    use HasFactory;
    protected $table = 'customer_wallet_credits';
    public $timestamps = true;
    protected $primaryKey = 'id';
}