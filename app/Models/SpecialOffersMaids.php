<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpecialOffersMaids extends Model
{
    use HasFactory;
    protected $table = 'special_offers_maids';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
