ALTER TABLE `bookings` ADD `supervisor_selected` ENUM('N','Y') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'N' COMMENT 'Y - opted for supervisor' AFTER `_total_payable`;


ALTER TABLE bookings ADD supervisor_charge_hourly DECIMAL(10,2) UNSIGNED NULL DEFAULT '0.00' AFTER supervisor_selected;
ALTER TABLE bookings ADD supervisor_charge_total DECIMAL(10,2) UNSIGNED NULL DEFAULT '0.00' AFTER supervisor_charge_hourly;
ALTER TABLE bookings ADD transfer_type VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL AFTER tabletid, ADD INDEX transfer_type (transfer_type);
ALTER TABLE bookings ADD moved_from_booking_id BIGINT(20) UNSIGNED NULL DEFAULT NULL AFTER subscription_odoo_id, ADD moved_to_booking_id BIGINT(20) UNSIGNED NULL DEFAULT NULL AFTER moved_from_booking_id, ADD INDEX moved_from_booking_id (moved_from_booking_id), ADD INDEX moved_to_booking_id (moved_to_booking_id);
ALTER TABLE bookings ADD plan_based_supplies TINYINT(3) UNSIGNED NULL DEFAULT '0' COMMENT '1 - Selected any plan based supplies' AFTER moved_to_booking_id, ADD plan_based_supplies_amount DECIMAL(10,2) UNSIGNED NULL DEFAULT '0.00' AFTER plan_based_supplies, ADD custom_supplies TINYINT(3) UNSIGNED NULL DEFAULT '0' COMMENT '1 - Selected any custom supplies' AFTER plan_based_supplies_amount, ADD custom_supplies_amount DECIMAL(10,2) UNSIGNED NULL DEFAULT '0.00' AFTER custom_supplies;
ALTER TABLE bookings ADD is_free ENUM('N','Y') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'N' COMMENT 'Y -Yes,N-No' AFTER web_book_show, ADD INDEX is_free (is_free);