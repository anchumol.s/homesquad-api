ALTER TABLE `service_types`
ADD `web_url_slug` varchar(255) NULL;

ALTER TABLE `service_types`
ADD `info_html` text COLLATE 'latin1_swedish_ci' NULL;


ALTER TABLE `package`
ADD `tc_html` text NULL;



ALTER TABLE `offers_sb`
CHANGE `offer_timestamp` `offer_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE CURRENT_TIMESTAMP AFTER `offer_status`,
ADD `coupon_id` bigint(20) NULL,
ADD FOREIGN KEY (`coupon_id`) REFERENCES `coupon_code` (`coupon_id`);



ALTER TABLE `payment_types`
ADD `customer_app_icon` varchar(255) NULL,
ADD `sort_order` int(11) NULL AFTER `customer_app_icon`,
ADD `default` tinyint(4) NULL AFTER `sort_order`,
ADD `show_in_web` tinyint(1) NULL AFTER `default`,
ADD `show_in_app` tinyint(1) NULL AFTER `show_in_web`;


ALTER TABLE `offers_sb`
ADD `banner_image` varchar(250) COLLATE 'utf8_general_ci' NULL AFTER `offer_image`,
CHANGE `offer_timestamp` `offer_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE CURRENT_TIMESTAMP AFTER `offer_status`;




UPDATE `offers_sb` SET
`id_offers_sb` = '27',
`offer_heading` = 'Welcome ',
`offer_type` = 'Text Type',
`offer_subheading` = 'Welcome ',
`offer_content` = 'test ',
`offer_image` = '',
`banner_image` = NULL,
`offer_status` = '1',
`offer_timestamp` = now(),
`coupon_id` = '15'
WHERE `id_offers_sb` = '27';


UPDATE `offers_sb` SET
`id_offers_sb` = '27',
`offer_heading` = 'Welcome ',
`offer_type` = 'Text Type',
`offer_subheading` = 'Welcome ',
`offer_content` = 'test ',
`offer_image` = '',
`banner_image` = 'offer-22.jpg',
`offer_status` = '1',
`offer_timestamp` = now(),
`coupon_id` = '15'
WHERE `id_offers_sb` = '27';



ALTER TABLE `package`
ADD `tax_method` enum('I','E') COLLATE 'utf8_general_ci' NULL;



INSERT INTO `offers_sb` (`id_offers_sb`, `coupon_id`, `offer_heading`, `offer_type`, `offer_subheading`, `offer_content`, `offer_image`, `banner_image`, `offer_status`, `offer_timestamp`) VALUES
(2,	14,	'20% on upholestry Cleaning ',	'Text Type',	'Use promo Code SOFA20 for 20% off professional sofa deep cleaning.Limited period offer. Book Now',	'Most reliable sofa cleaning service in Dubai, with trusted & professional cleaners, and systematic cleaning procedures. We use Professional Machines and leave your sofa as new, wether your sofa is fabric, leather or suede we have the right solution for every material. ',	'',	'offer-32.jpg',	0,	'2023-12-13 08:44:52'),
(3,	NULL,	'hi',	'Text Type',	'Welcome ',	'test',	'',	NULL,	1,	'2022-12-14 05:13:40'),

(4,	15,	'Reduced deep cleaning prices',	'Text Type',	'Use promocode MOVE20 for 20% off on Deep cleaning any day of the week. Limited Offer. book Now!',	'Our professional cleaners will deep clean every room in your home, including hard-to-reach areas like ceiling, chandeliers and AC vents We\'ll sanitize all surfaces and clean your kitchen, bathroom, living room, and bedroom. We also offer a deep cleaning service for those tough spots. Call us today to schedule an appointment and enjoy a spotless and fresh home!',	'',	'offer-22.jpg',	0,	'2023-12-13 08:44:52'),
(5,	NULL,	'New Payment Option!!!',	'Text Type',	'New Payment Option!!!',	'Woohoo!! We\'ve got some exciting news to share - you can now pay for our services using Apple Pay! Say goodbye to the hassle of entering payment information and enjoy a faster checkout experience. Simply select Apple Pay at checkout and voila - payment complete! Thank you for choosing our services, and we look forward to serving you again soon.',	'',	NULL,	1,	'2023-12-01 10:20:56'),
(6,	16,	'Back-to-School House Cleaning Offer! ',	'Text Type',	'Don\'t miss out on this fantastic deal. Book now using code BTS30 and enjoy a cleaner home while saving 30%',	'Is your home ready for the new school year? Let us help you get it in tip-top shape with our exclusive 30% discount on house cleaning services! ',	'',	NULL,	1,	'2023-12-02 14:59:40'),
(7,	14,	'Back to School House Cleaning Offer',	'Text Type',	'Don\'t miss out on this fantastic deal. Book now using code BTS30 and enjoy a cleaner home while saving 30%',	'Is your home ready for the new school year? Let us help you get it in tip-top shape with 30% discount on house cleaning services',	'',	NULL,	1,	'2023-12-02 14:59:40');



ALTER TABLE `building_type_room_packages` ADD `no_of_maids` INT NOT NULL DEFAULT '1' AFTER `deleted_at`;


ALTER TABLE `building_type_room_packages`
ADD `cart_limit` int(11) NOT NULL DEFAULT '1',
COMMENT='maximum count allowed to add on cart';

ALTER TABLE `building_type_room_packages`
CHANGE `cart_limit` `cart_limit` int(11) NOT NULL DEFAULT '1' COMMENT 'maximum count allowed to add on cart' AFTER `no_of_maids`,
COMMENT='';

ALTER TABLE `building_type_room_packages`
ADD `sort_order` int(11) NOT NULL DEFAULT '1';


INSERT INTO `customer_addresses` (`customer_address_id`, `customer_id`, `area_id`, `other_area`, `customer_address`, `latitude`, `longitude`, `building`, `unit_no`, `street`, `default_address`, `address_category`, `address_status`, `home_type`) VALUES
(1,	2,	48,	NULL,	'D1 Tower at Jaddaf Waterfront - ?44 - Dubai - United Arab Emirates',	'',	'',	'Apt 2705',	'Apt 2705',	'',	1,	NULL,	0,	NULL),
(2,	3,	11,	NULL,	'Royal Residence 1 - Dubai - United Arab Emirates',	'',	'',	'Apt 1015',	'Apt 1015',	NULL,	1,	NULL,	0,	NULL),
(3,	4,	66,	NULL,	'Building/Villa - Creek Horizon, Tower 2, Apt. 206',	'25.182208',	'55.4074112',	'',	'3206',	'Al Sufouh',	1,	'HO',	0,	NULL),
(4,	5,	54,	NULL,	'Building/Villa - Flat, Unit No - 3602, Street - Almustaqbal Street',	'',	'',	'Flat',	'3602',	'Almustaqbal Street',	1,	NULL,	0,	NULL),
(7,	8,	22,	NULL,	'Building/Villa - Belgravia , Unit No - 139, Street - 10',	'',	'',	'Belgravia  1A',	'139',	'10',	1,	NULL,	0,	NULL),
(8,	11,	16,	NULL,	'Building/Villa - Villa 74 Cluster 2, Unit No - 74, Street - Villa 74 Cluster 2, The Sustainable City',	'',	'',	'',	'74',	'Villa 74 Cluster 2, The Sustainable City',	1,	NULL,	0,	NULL),
(9,	12,	34,	NULL,	'Belresheed Building',	'',	'',	'1403',	'201B',	'Street 1',	1,	NULL,	0,	NULL),
(10,	13,	57,	NULL,	'Building/Villa - Shoreline Apartments 10 (Al Das), Unit No - Apartment 507, Street - Shoreline Apartments 10 (Al Das)',	'',	'',	'Shoreline Apartments 10 (Al Das)',	'Apartment 507',	'Shoreline Apartments 10 (Al Das)',	1,	NULL,	0,	NULL),
(11,	1,	7,	NULL,	'Dubai - United Arab Emirates',	'25.2048493',	'55.2707828',	'102',	'11',	'11',	1,	NULL,	1,	NULL),
(12,	14,	21,	NULL,	'Building/Villa - 406, Unit No - 406, Street - Jvc prime buisiness center ,office 201b',	'',	'',	'3904',	'406',	'Jvc prime buisiness center ,office 201b',	1,	NULL,	0,	NULL),
(14,	17,	52,	NULL,	'Zaafaran 5 - Dubai - United Arab Emirates',	'',	'',	'G12',	'G12',	NULL,	1,	NULL,	0,	NULL),
(15,	18,	103,	NULL,	'Arjan, Q Gardens Boutique Residences, Block A, Apartment 215',	'',	'',	'Apartment 215',	'Apartment 215',	NULL,	1,	NULL,	0,	NULL),
(16,	19,	1,	NULL,	'Building 107 ',	'',	'',	'28',	'28',	NULL,	1,	NULL,	0,	NULL),
(17,	20,	42,	NULL,	'Diyafa building - Dubai - United Arab Emirates',	'',	'',	'106',	'106',	NULL,	1,	NULL,	0,	NULL),
(18,	21,	55,	NULL,	'Marina Crown - Dubai - United Arab Emirates',	'',	'',	'1512',	'1512',	NULL,	1,	NULL,	1,	NULL),
(19,	22,	29,	NULL,	'District 3',	'',	'',	'L42',	'L42',	NULL,	1,	NULL,	0,	NULL),
(20,	23,	34,	NULL,	'Horizon Tower - ???? ?????? - Dubai - United Arab Emirates',	'',	'',	'304',	'304',	NULL,	1,	NULL,	0,	NULL),
(21,	24,	57,	NULL,	'Marina Residence 2',	'',	'',	'1006',	'1006',	NULL,	1,	NULL,	0,	NULL);


UPDATE `coupon_code` SET
`coupon_id` = '15',
`coupon_name` = 'WEEKLY',
`service_id` = '42',
`percentage` = '10',
`type` = 'F',
`coupon_type` = NULL,
`offer_type` = '',
`discount_type` = '0',
`valid_week_day` = '',
`min_hrs` = '0',
`expiry_date` = '0000-00-00',
`added_date` = '0000-00-00',
`status` = '1'
WHERE `coupon_id` = '15';

INSERT INTO `coupon_code` (`coupon_id`, `coupon_name`, `service_id`, `percentage`, `type`, `coupon_type`, `offer_type`, `discount_type`, `valid_week_day`, `min_hrs`, `expiry_date`, `added_date`, `status`) VALUES
(4,	'DEEP10',	25,	'20',	'C',	'ET',	'F',	0,	'1,2,3,4,6',	6,	'2024-12-31',	'2023-12-06',	1);

UPDATE `coupon_code` SET
`coupon_id` = '4',
`coupon_name` = 'DEEP10',
`service_id` = '42',
`percentage` = '20',
`type` = 'C',
`coupon_type` = 'ET',
`offer_type` = 'F',
`discount_type` = '0',
`valid_week_day` = '1,2,3,4,6',
`min_hrs` = '6',
`expiry_date` = '2024-12-31',
`added_date` = '2023-12-06',
`status` = '1'
WHERE `coupon_id` = '4';


UPDATE `coupon_code` SET
`coupon_id` = '4',
`coupon_name` = 'DEEP10',
`service_id` = '42',
`percentage` = '20',
`type` = 'C',
`coupon_type` = 'ET',
`offer_type` = 'F',
`discount_type` = '0',
`valid_week_day` = '1,2,3,4,6',
`min_hrs` = '2',
`expiry_date` = '2024-12-31',
`added_date` = '2023-12-06',
`status` = '1'
WHERE `coupon_id` = '4';


INSERT INTO `coupon_code` (`coupon_id`, `coupon_name`, `service_id`, `percentage`, `type`, `coupon_type`, `offer_type`, `discount_type`, `valid_week_day`, `min_hrs`, `expiry_date`, `added_date`, `status`) VALUES
(100,	'BACK',	36,	'20%',	'C',	'OT',	'F',	0,	'0,1,2,3,4,5,6',	2,	'2023-09-09',	'2023-09-02',	0);

UPDATE `coupon_code` SET
`coupon_id` = '100',
`coupon_name` = 'BACK',
`service_id` = '36',
`percentage` = '20%',
`type` = 'C',
`coupon_type` = 'OT',
`offer_type` = 'F',
`discount_type` = '0',
`valid_week_day` = '0,1,2,3,4,5,6',
`min_hrs` = '2',
`expiry_date` = '2023-09-09',
`added_date` = '2023-09-02',
`status` = '1'
WHERE `coupon_id` = '100';


UPDATE `coupon_code` SET
`coupon_id` = '100',
`coupon_name` = 'BACK',
`service_id` = '36',
`percentage` = '20%',
`type` = 'C',
`coupon_type` = 'OT',
`offer_type` = 'F',
`discount_type` = '0',
`valid_week_day` = '0,1,2,3,4,5,6',
`min_hrs` = '2',
`expiry_date` = '2024-09-09',
`added_date` = '2023-09-02',
`status` = '1'
WHERE `coupon_id` = '100';

UPDATE `coupon_code` SET
`coupon_id` = '4',
`coupon_name` = 'DEEP10',
`service_id` = '36',
`percentage` = '20',
`type` = 'C',
`coupon_type` = 'ET',
`offer_type` = 'F',
`discount_type` = '0',
`valid_week_day` = '1,2,3,4,6',
`min_hrs` = '2',
`expiry_date` = '2024-12-31',
`added_date` = '2023-12-06',
`status` = '1'
WHERE `coupon_id` = '4';

ALTER TABLE `bookings`
ADD `_total_payable` decimal(10,2) unsigned NULL DEFAULT '0.00';

ALTER TABLE `bookings`
CHANGE `_total_payable` `_total_payable` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' AFTER `subscription_package_id`;



CREATE TABLE `booking_subscription_packages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `booking_id` bigint(20) unsigned NOT NULL,
  `package_id` bigint(20) unsigned NOT NULL,
  `package_name` varchar(255) NOT NULL,
  `cleaning_material` enum('Y','N') NOT NULL DEFAULT 'N',
  `no_of_bookings` int(10) unsigned NOT NULL,
  `working_hours` double(5,2) unsigned NOT NULL,
  `booking_type` enum('MO','WE','YE') NOT NULL,
  `strikethrough_amount` double(10,2) unsigned NOT NULL,
  `amount` double(10,2) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `booking_id` (`booking_id`),
  KEY `package_id` (`package_id`),
  CONSTRAINT `booking_subscription_packages_ibfk_1` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`),
  CONSTRAINT `booking_subscription_packages_ibfk_2` FOREIGN KEY (`package_id`) REFERENCES `package` (`package_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


INSERT INTO `coupon_code` (`coupon_id`, `coupon_name`, `service_id`, `percentage`, `type`, `coupon_type`, `offer_type`, `discount_type`, `valid_week_day`, `min_hrs`, `expiry_date`, `added_date`, `status`) VALUES
(12,	'DEEP10',	25,	'20',	'C',	'ET',	'F',	0,	'1,2,3,4,6',	6,	'2024-12-31',	'2023-12-06',	1);

UPDATE `coupon_code` SET
`coupon_id` = '12',
`coupon_name` = 'DEEP10',
`service_id` = '53',
`percentage` = '20',
`type` = 'C',
`coupon_type` = 'ET',
`offer_type` = 'F',
`discount_type` = '0',
`valid_week_day` = '1,2,3,4,6',
`min_hrs` = '6',
`expiry_date` = '2024-12-31',
`added_date` = '2023-12-06',
`status` = '1'
WHERE `coupon_id` = '12';



UPDATE `coupon_code` SET
`coupon_id` = '12',
`coupon_name` = 'DEEP1',
`service_id` = '53',
`percentage` = '20',
`type` = 'C',
`coupon_type` = 'ET',
`offer_type` = 'F',
`discount_type` = '0',
`valid_week_day` = '1,2,3,4,6',
`min_hrs` = '6',
`expiry_date` = '2024-12-31',
`added_date` = '2023-12-06',
`status` = '1'
WHERE `coupon_id` = '12';


UPDATE `coupon_code` SET
`coupon_id` = '12',
`coupon_name` = 'DEEP1',
`service_id` = '53',
`percentage` = '20',
`type` = 'C',
`coupon_type` = 'ET',
`offer_type` = 'F',
`discount_type` = '0',
`valid_week_day` = '1,2,3,4,6',
`min_hrs` = '2',
`expiry_date` = '2024-12-31',
`added_date` = '2023-12-06',
`status` = '1'
WHERE `coupon_id` = '12';


INSERT INTO service_type_models (id, model) VALUES
(3, 'enquiry');

UPDATE `payment_types` SET
`id` = '1',
`name` = 'Cash',
`code` = 'cash',
`description` = NULL,
`charge` = '10.00',
`charge_type` = 1,
`deleted_at` = NULL,
`customer_app_icon` = 'cash.png',
`sort_order` = NULL,
`default` = NULL,
`show_in_web` = '1',
`show_in_app` = '1'
WHERE `id` = '1';

ALTER TABLE `bookings`
ADD `_service_amount` decimal(10,2) unsigned NULL DEFAULT '0.00';


ALTER TABLE `bookings`
ADD `_cleaning_materials_amount` decimal(10,2) unsigned NULL DEFAULT '0.00';

ALTER TABLE `bookings`
ADD `_service_addons_amount` decimal(10,2) unsigned NULL DEFAULT '0.00';

ALTER TABLE `bookings`
ADD `_packages_amount` decimal(10,2) unsigned NULL DEFAULT '0.00';

ALTER TABLE `bookings`
ADD `_subscription_package_amount` decimal(10,2) unsigned NULL DEFAULT '0.00';

ALTER TABLE `bookings`
ADD `_amount_before_discount` decimal(10,2) unsigned NULL DEFAULT '0.00';

ALTER TABLE `bookings`
ADD `_coupon_discount` decimal(10,2) unsigned NULL DEFAULT '0.00';

ALTER TABLE `bookings`
ADD `_discount_total` decimal(10,2) unsigned NULL DEFAULT '0.00';

ALTER TABLE `bookings`
ADD `_taxable_amount` decimal(10,2) unsigned NULL DEFAULT '0.00';

ALTER TABLE `bookings`
ADD `_vat_percentage` decimal(10,2) unsigned NULL DEFAULT '0.00';

ALTER TABLE `bookings`
ADD `_vat_amount` decimal(10,2) unsigned NULL DEFAULT '0.00';

ALTER TABLE `bookings`
ADD `_taxed_amount` decimal(10,2) unsigned NULL DEFAULT '0.00';

ALTER TABLE `bookings`
ADD `_payment_type_charge` decimal(10,2) unsigned NULL DEFAULT '0.00';